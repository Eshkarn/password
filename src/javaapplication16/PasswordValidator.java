/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eshka
 */
public class PasswordValidator {

	public boolean validate(String password) {
		boolean isCapital = false, hasSpecialChar=false, hasDigit=false,isValid = false;
		char[] specialChars = {'@', '$', '+', '!', '#', '?', '^', '&'};
		int passwordLength = password.length();
		
		//Check for length
		if(passwordLength < 8) {
			return false;
		}
		
		//loop every character in password
		for(int i=0; i<passwordLength; i++) {
			char chr = password.charAt(i);
			int asciiValue = chr;
			
			//Check for capital letters
			if(asciiValue >= 65 && asciiValue <=90) {
				isCapital = true;
			}
			
			//Check for special characters
			for(int j=0; j<8; j++) {
				if(chr == specialChars[j]) {
					hasSpecialChar = true;
				}
			}
			
			//Check for digit
			for(int k=48; k<=57; k++) {
				char num = (char)k;
				if(chr == num) {
					hasDigit = true;
				}
			}
		}
		
		//Check if all conditions are met
		if(isCapital && hasSpecialChar && hasDigit) {
			isValid = true;
		}
			
		return isValid;
		}
}

